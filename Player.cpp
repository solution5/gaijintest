#include "Player.h"

/**
 * \brief This is a constructor of player platform.
 * \param x The coordinates for X axis.
 * \param y The coordinates for Y axis.
 * \param width The value witch contains width of geometry primitives.
 * \param height The value witch contains height of geometry primitives.
 */
Player::Player(unsigned x, unsigned y, unsigned width, unsigned height):GeometryBase(x, y, width, height)
{
}

/**
 * \brief This is a function that moves player to left.
 */
void Player::MoveLeft()
{
    if(this->x > 0)
    {
        this->x -= Speed;
    }
}

/**
 * \brief This is a function that moves player to right.
 */
void Player::MoveRight()
{
    if (this->x < Max)
    {
        this->x += Speed;
    }
}

/**
 * \brief A function that sets the maximum value for player.
 * \param MaxValue 
 */
void Player::SetMax(unsigned MaxValue)
{
    this->Max = MaxValue;
}

/**
 * \brief Returning the color of player.
 * \return unsigned color value.
 */
unsigned Player::GetColor()
{
    return this->Color;
}

