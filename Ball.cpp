#include "Ball.h"

Ball::Ball(int x, int y, int width, int height) : GeometryBase(x, y, width, height)
{
    
}

void Ball::Move()
{
    int NextX = x + Speed * DirectionX;
    int NextY = y + Speed * DirectionY;

    if (NextX + Width > MaxX)
    {
        ChangeDirectionX();
        return;
    }
    else if (NextX <= 0)
    {
        ChangeDirectionX();
        return;
    }

    if (NextY + Height > MaxY)
    {
        GameOver = true;
        return;
    } else if (NextY <= 0)
    {
        ChangeDirectionY();
        return;
    }
    x = NextX;
    y = NextY;

}

void Ball::SetMax(int NewX, int NewY)
{
    MaxX = NewX;
    MaxY = NewY;
}

void Ball::SetSpeed(int NewSpeed)
{
    this->Speed = NewSpeed;
}

void Ball::IncrementingSpeed()
{
    if (this->Speed <= this->MaxSpeed)
    {
        this->Speed++;
    }
}

void Ball::DecrementingSpeed()
{
    if (this->Speed > 1)
    {
        this->Speed--;
    }
}

bool Ball::bIsGameOver()
{
    return this->GameOver;
}

void Ball::ChangeDirectionX()
{
    DirectionX = -DirectionX;
}

void Ball::ChangeDirectionY()
{
    DirectionY = -DirectionY;
}

unsigned Ball::GetColor()
{
    return this->Color;
}
