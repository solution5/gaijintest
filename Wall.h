#pragma once
#include "GeometryBase.h"
#include <vector>

class Wall : GeometryBase
{
    int BlockSizeX = 40;
    int BlockSizeY = 20;
    int Indent = 10;
    int Count = 19;
public:
    std::vector<GeometryBase> Blocks;
    Wall(int x, int y);
};
