#pragma once+
#include <stdint.h>

class DrawerGraphics
{
public:
    void DrawRectangle(uint32_t *buf, unsigned svw, unsigned svh,
    unsigned x, unsigned y, unsigned width, unsigned height, unsigned color);

};