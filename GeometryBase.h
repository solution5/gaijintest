#pragma once
/**
 * \brief Main class witch creat geometry primitives.
 */
class GeometryBase
{
protected:
    int x;
    int y;
    int Width;
    int Height;
    unsigned Color;
public:
    /*This is a constructor.*/
    GeometryBase(int x = 0, int y = 0, int width = 0, int height = 0);

    /*A getter method witch get X coordinates.*/
    int GetX();

    /*A getter method witch get Y coordinates.*/
    int GetY();

    /*A getter method witch get width object.*/
    int GetWidth();

    /*A getter method witch get height object.*/
    int GetHeight();
};

