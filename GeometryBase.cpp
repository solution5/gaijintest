#include "GeometryBase.h"

/**
 * \brief This is a constructor of geometry primitives.
 * \param x The coordinates for X axis.
 * \param y The coordinates for Y axis.
 * \param width The value witch contains width of geometry primitives.
 * \param height The value witch contains height of geometry primitives.
 */
GeometryBase::GeometryBase(int x, int y, int width, int height)
{
    this->x = x;
    this->y = y;
    this->Width = width;
    this->Height = height;
}

/**
 * \brief A getter method witch get X coordinates.
 * \return get integer X coordinates.
 */
int GeometryBase::GetX()
{
    return x;
}

/**
 * \brief A getter method witch get Y coordinates.
 * \return get integer Y coordinates.
 */
int GeometryBase::GetY()
{
    return y;
}

/**
 * \brief A getter method witch get width.
 * \return get integer width.
 */
int GeometryBase::GetWidth()
{
    return Width;
}

/**
 * \brief A getter method witch get height.
 * \return get integer height.
 */
int GeometryBase::GetHeight()
{
    return Height;
}
