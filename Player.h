#pragma once

#include "GeometryBase.h"

/**
 * \brief Creating a class called Player that inherits from GeometryBase.
 * It has a constructor that takes in 4 parameters.
 */
class Player: public GeometryBase
{
    unsigned Speed = 1;
    unsigned Max = 0;
    unsigned Color = 0xFFFFFFF00;
public:
    /*This is a constructor.*/
    Player(unsigned x=0, unsigned y=0, unsigned width=0, unsigned height=0);

    /*It has a method called MoveRight that does moving right geometry primitive.*/
    void MoveLeft();

    /*It has a method called MoveRight that does moving left geometry primitive.*/
    void MoveRight();

    /*It has a method called SetMax that takes in a parameter and sets the Max variable to that value.*/
    void SetMax(unsigned MaxValue);

    /*It has a method called GetColor that returns the Color variable.*/
    unsigned GetColor();
};

