#pragma once
#include "GeometryBase.h"
#include <vector>

class Ball : public GeometryBase
{
    int DirectionX = 1;
    int DirectionY = -1;
    int Speed = 7;
    int MaxSpeed = 10;
    int MaxX = 0;
    int MaxY = 0;
    unsigned Color = 0xFFFFFFFF;
    bool GameOver = false;

    std::vector<Ball> BallsSet;
    int BallSize = 15;
    int Indent = 10;
    int Count = 3;
public:
    Ball(int x = 0, int y = 0, int width = 0, int height = 0);
    void Move();
    void SetMax(int NewX, int NewY);
    void SetSpeed(int NewSpeed);
    void IncrementingSpeed();
    void DecrementingSpeed();
    bool bIsGameOver();
    void ChangeDirectionX();
    void ChangeDirectionY();
    unsigned GetColor();
};

