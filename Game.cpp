#include "Engine.h"
#include <stdlib.h>
#include <memory.h>
#include <vector>
#include "GeometryBase.h"
#include "DrawerGraphics.h"
#include "Player.h"
#include "Ball.h"
#include "Wall.h"

int NumberBalls = 0;

DrawerGraphics Drawer;
Player PlayerPlatform;
std::vector<Ball*> AllBals;
Wall *WallBlocks;

//
//  You are free to modify this file
//

//  is_key_pressed(int button_vk_code) - check if a key is pressed,
//                                       use keycodes (VK_SPACE, VK_RIGHT, VK_LEFT, VK_UP, VK_DOWN, 'A', 'B')
//
//  get_cursor_x(), get_cursor_y() - get mouse cursor position
//  is_mouse_button_pressed(int button) - check if mouse button is pressed (0 - left button, 1 - right button)
//  clear_buffer() - set all pixels in buffer to 'black'
//  is_window_active() - returns true if window is active
//  schedule_quit_game() - quit game after act()

void SpawnBall()
{
  NumberBalls++;
  int BallSize = 15;
  int RelativeBallSizeWidth = (SCREEN_WIDTH - BallSize) / 2;
  int RelativeBallSizeHeight = (SCREEN_HEIGHT - BallSize) / 2;

  Ball *NewBall = new Ball(RelativeBallSizeWidth, RelativeBallSizeHeight, BallSize, BallSize);
  NewBall->SetMax(SCREEN_WIDTH, SCREEN_HEIGHT);
  AllBals.push_back(NewBall);
}

void StartGame()
{
  //set start place
  int PlayerSizeWidth = 50;
  int PlayerSizeHeight = 20;
  int RelativePlayerSizeWidth = (SCREEN_WIDTH - PlayerSizeWidth) / 2;
  int RelativePlayerSizeHeight = SCREEN_HEIGHT - PlayerSizeHeight * 2;
	
  PlayerPlatform = Player(RelativePlayerSizeWidth,RelativePlayerSizeHeight, PlayerSizeWidth, PlayerSizeHeight);
  PlayerPlatform.SetMax(SCREEN_WIDTH);

  int BallSize = 15;
  int RelativeBallSizeWidth = (SCREEN_WIDTH - BallSize) / 2;
  int RelativeBallSizeHeight = (SCREEN_HEIGHT - BallSize) / 2;
  
  NumberBalls = 0;
  SpawnBall();
	
  WallBlocks = new Wall(30, 50);
}

// initialize game data in this function
void initialize()
{
  StartGame();
}

// fill buffer in this function
// uint32_t buffer[SCREEN_HEIGHT][SCREEN_WIDTH] - is an array of 32-bit colors (8 bits per R, G, B)
void draw()
{
  if (!buffer)
  {
    return;
  }
  
  // clear backbuffer
  memset(buffer, 0, SCREEN_HEIGHT * SCREEN_WIDTH * sizeof(uint32_t));
  Drawer.DrawRectangle(buffer, SCREEN_WIDTH, SCREEN_HEIGHT, PlayerPlatform.GetX(), PlayerPlatform.GetY(), PlayerPlatform.GetWidth(), PlayerPlatform.GetHeight(), PlayerPlatform.GetColor());

  for (int i = 0; i < AllBals.size(); ++i)
  {
    Ball *NewBall = AllBals.at(i);
    Drawer.DrawRectangle(buffer, SCREEN_WIDTH, SCREEN_HEIGHT, NewBall->GetX(), NewBall->GetY(), NewBall->GetWidth(), NewBall->GetHeight(), NewBall->GetColor());
  }
  
  for (GeometryBase IteratorBlocks : WallBlocks->Blocks)
  {
    Drawer.DrawRectangle(buffer, SCREEN_WIDTH, SCREEN_HEIGHT, IteratorBlocks.GetX(), IteratorBlocks.GetY(), IteratorBlocks.GetWidth(), IteratorBlocks.GetHeight(), 0x11111FF00);
  }

}

/**
 * \brief Checking event overlapping between two actors
 * \param first First actor overlapping
 * \param second Second actor overlapping
 * \return bool value. True if overlapped. False if not overlapped.
 */
bool OverlapEvent(GeometryBase* first, GeometryBase* second)
{
  if (second->GetY() + second->GetHeight() > first->GetY() && second->GetY() < first->GetY() + first->GetHeight())
  {

    if (((second->GetX() > first->GetX() &&
      second->GetX() < first->GetX() + first->GetWidth()) ||
      (second->GetX() + second->GetWidth() > first->GetX() &&
        second->GetX() + second->GetWidth() < first->GetX() + first->GetWidth())))
    {
      return true;
    }
  }
  return false;
}

void OverlapWallEvent()
{
  for (int i = 0; i < AllBals.size(); ++i)
  {
    Ball *NewBall = AllBals.at(i);
    if (OverlapEvent(&PlayerPlatform, &*NewBall))
    {
      NewBall->ChangeDirectionY();
      NewBall->Move();
    }
  }
}

// Have bug all balls erase from vector.
void DeadEvent()
{
  for (int i = 0; i < AllBals.size(); ++i)
  {
    Ball *NewBall = AllBals.at(i);
    
    if (NewBall->bIsGameOver())
    {
      AllBals.erase(AllBals.begin(), AllBals.end() - i);
      if (AllBals.size() == 0)
      {
        StartGame();
      }
    }
  }
}

// Instead of falling bonuses
void AssignmentBonusSpeed()
{
  for (int i = 0; i < AllBals.size(); ++i)
  {
    Ball *NewBall = AllBals.at(i);
    
    int bonus = rand() % 3;
    if (bonus == 1)
    {
      NewBall->IncrementingSpeed();
    }
    else if (bonus == 2)
    {
      SpawnBall();
      break;
    }
    else
    {
      NewBall->DecrementingSpeed();
    }
  }
}

// this function is called to update game data,
// dt - time elapsed since the previous update (in seconds)
void act(float dt)
{
  if (is_key_pressed(VK_ESCAPE))
  {
    schedule_quit_game();
  }
  if (is_key_pressed(VK_LEFT))
  {
    PlayerPlatform.MoveLeft();
  }
  
  if (is_key_pressed(VK_RIGHT))
  {
    PlayerPlatform.MoveRight();
  }

  static const  float TimeInterval = 0.03;
  static float TimeToMove = 0;
  TimeToMove += dt;
  //move every time_interval seconds
  if (TimeToMove > TimeInterval)
  {
    for (int i = 0; i < AllBals.size(); ++i)
    {
      Ball *NewBall = AllBals.at(i);
      NewBall->Move();
      TimeToMove = 0;

      
      OverlapWallEvent();
		  
      for (auto block = WallBlocks->Blocks.begin(); block != WallBlocks->Blocks.end(); ++block)
      {
        if (OverlapEvent(&(*block), &*NewBall))
        {
          NewBall->ChangeDirectionY();
          WallBlocks->Blocks.erase(block);
          AssignmentBonusSpeed();
          break;
        }
      }

      DeadEvent();
    }
  }
}

// free game data in this function
void finalize()
{
  clear_buffer();
}

