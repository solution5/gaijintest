
#include "Wall.h"

Wall::Wall(int x, int y)
{
    for (size_t i = 0; i < Count; i++)
    {
        Blocks.push_back(GeometryBase(x + (BlockSizeX + Indent) * i, y, BlockSizeX, BlockSizeY));
    }
}
